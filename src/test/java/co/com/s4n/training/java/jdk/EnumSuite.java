package co.com.s4n.training.java.jdk;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class EnumSuite {
    enum Color
    {
        RED, GREEN, BLUE;
    }


    @Test
    public void smokeTest() {
        assertTrue(true);
    }

    @Test
    public void Testenum() {

            Color c1 = Color.RED;
            System.out.println(c1);

        assertEquals("RED",c1.toString());
        }

    enum Day
    {
        SUNDAY, MONDAY, TUESDAY, WEDNESDAY,
        THURSDAY, FRIDAY, SATURDAY;
    }
    public class EnumTest {
        Day day;

        public EnumTest(Day day) {
            this.day = day;
        }

        public void tellItLikeItIs() {
            switch (day) {
                case MONDAY:
                    System.out.println("Mondays are bad.");
                    break;

                case FRIDAY:
                    System.out.println("Fridays are better.");
                    break;

                case SATURDAY:
                case SUNDAY:
                    System.out.println("Weekends are best.");
                    break;

                default:
                    System.out.println("Midweek days are so-so.");
                    break;
            }
        }
    }
    @Test
    public void Testday() {
        EnumTest firstDay = new EnumTest(Day.MONDAY);
        firstDay.tellItLikeItIs();

    }

    public PizzaStatus status;
    public enum PizzaStatus {
        ORDERED,
        READY,
        DELIVERED;
    }

    @Test
    public void Teststatus() {
        PizzaStatus p1 = PizzaStatus.READY;
        if ("READY" == PizzaStatus.READY.toString()) {
            System.out.println(p1);
        }


    }


}
