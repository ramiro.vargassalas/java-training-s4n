package co.com.s4n.training.java.jdk;

import static org.junit.Assert.*;
import org.junit.Test;
import java.util.function.BiFunction;
import java.util.function.Consumer;
//import java.util.function.IntBinaryOperator;
import java.util.function.DoubleBinaryOperator;
import java.util.function.Supplier;

public class LambdaSuite {
    //solo puede tener un metodo
    @FunctionalInterface
    interface InterfaceDeEjemplo{
        int metodoDeEjemplo(int x, int y);
        //int ejemplo(int a, int b);//
    }
    // creacion de bi-functions
    class ClaseDeEjemplo{
        public int metodoDeEjemplo1(int z, InterfaceDeEjemplo i){
            return z + i.metodoDeEjemplo(1,2);
        }

        public int metodoDeEjemplo2(int z, BiFunction<Integer, Integer, Integer> fn){
            return z + fn.apply(1,2);
        }
    }

    @Test
    public void smokeTest() {
        assertTrue(true);
    }

    //usa la interfaz funcional
    @Test
    public void usarUnaInterfaceFuncional1(){

        InterfaceDeEjemplo i = (x,y)->x+y;

        ClaseDeEjemplo instancia = new ClaseDeEjemplo();

        int resultado = instancia.metodoDeEjemplo1(1,i);

        assertTrue(resultado==4);
    }

    @Test
    public void usarUnaInterfaceFuncional2(){

        BiFunction<Integer, Integer, Integer> f = (x, y) -> new Integer(x.intValue()+y.intValue());

        ClaseDeEjemplo instancia = new ClaseDeEjemplo();

        int resultado = instancia.metodoDeEjemplo2(1,f);

        assertTrue(resultado==4);
    }

    class ClaseDeEjemplo2{

        public double metodoDeEjemplo2(double x, double y, DoubleBinaryOperator fn){
            return fn.applyAsDouble(x,y);
        }
    }
    @Test
    public void usarUnaFuncionConTiposPrimitivos(){
        DoubleBinaryOperator f = (x, y) -> x + y;

        ClaseDeEjemplo2 instancia = new ClaseDeEjemplo2();

        double resultado = instancia.metodoDeEjemplo2(1,2,f);

        assertEquals(3,resultado, 0.0);
    }

    class ClaseDeEjemplo3{

        public String operarConSupplier(Supplier<Integer> s){
            return "El int que me han entregado es: " + s.get();
        }
    }
    // operando con suplyiers
    @Test
    public void usarUnaFuncionConSupplier(){
        Supplier s1 = () -> {
            System.out.println("Cuándo se evalúa esto? (1)");
            return 4;
        };

        Supplier s2 = () -> {
            System.out.println("Cuándo se evalúa esto? (2)");
            return 4;
        };

        ClaseDeEjemplo3 instancia = new ClaseDeEjemplo3();

        String resultado = instancia.operarConSupplier(s2);

        assertEquals("El int que me han entregado es: 4",resultado);
    }

    class ClaseDeEjemplo4{

        private int i = 0;

        public void operarConConsumer(Consumer<Integer> c){
            c.accept(i);
        }
    }

    @Test
    public void usarUnaFuncionConConsumer(){
        Consumer<Integer> c1 = x -> {
            System.out.println("Me han entregado este valor: "+x);
        };

        ClaseDeEjemplo4 instancia = new ClaseDeEjemplo4();

        instancia.operarConConsumer(c1);

    }

    // calculadora usando bi-functions
    public interface Calculadora{
        default double sumar(double a, double b, DoubleBinaryOperator d){
            return d.applyAsDouble(a,b);
        }
        default double restar(double a, double b, DoubleBinaryOperator d){
            return d.applyAsDouble(a,b);
        }
        default double multiplicar(double a, double b, DoubleBinaryOperator d){
            return d.applyAsDouble(a,b);
        }
        default double dividir(double a, double b, DoubleBinaryOperator d){
            return d.applyAsDouble(a,b);
        }
    }

    class ImplementarCalculadora implements Calculadora{
    }

    @Test
    public void TestDeCalculadora(){
        DoubleBinaryOperator fn = (a,b) -> a + b;
        DoubleBinaryOperator fn2 = (a,b) -> a - b;
        DoubleBinaryOperator fn3 = (a,b) -> a * b;
        DoubleBinaryOperator fn4 = (a,b) -> a / b;
        ImplementarCalculadora cl = new ImplementarCalculadora();
        double re = cl.sumar(1.0, 2.0, fn);
        double re2 = cl.restar(1.0, 2.0, fn2);
        double re3 = cl.multiplicar(1.0, 2.0, fn3);
        double re4 = cl.dividir(1.0, 2.0, fn4);
        assertEquals(3.0, re, 0.0);
        assertEquals(-1.0, re2, 0.0);
        assertEquals(2, re3, 0.0);
        assertEquals(0.5, re4, 0.0);
    }


}