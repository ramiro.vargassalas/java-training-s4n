package co.com.s4n.training.java.jdk;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.*;

public class CompletableFutureSuite {

    private void sleep(int milliseconds){
        try{
            Thread.sleep(milliseconds);
        }catch(Exception e){
            System.out.println("Problemas durmiendo hilo");
        }
    }

    @Test
    public void t1() {

        //creacion de un completable future
        CompletableFuture<String> completableFuture = new CompletableFuture<>();

        //crea campos de hilos y nos ayuda a ejecutar tareas asincronicamente
        ExecutorService executorService = Executors.newFixedThreadPool(2);

        executorService.submit(() -> {
            System.out.println("t1 hilo diferente al de la ejecucion"+Thread.currentThread().getName());
            sleep(300);

            completableFuture.complete("Hello");
            return null;
        });
            System.out.println(Thread.currentThread().getName());

        try {
            String s = completableFuture.get(500, TimeUnit.MILLISECONDS);
            assertEquals(s, "Hello");
        }catch(Exception e){
            assertTrue(false);
        }finally{
            executorService.shutdown();

        }

    }



    @Test
    public void t2(){
        CompletableFuture<String> completableFuture
                = new CompletableFuture<>();

        ExecutorService executorService = Executors.newCachedThreadPool();

        executorService.submit(() -> {
            sleep(300);

            completableFuture.complete("Hello");
            return null;
        });

        try {
            String s = completableFuture.get(500, TimeUnit.MILLISECONDS);
            assertEquals(s, "Hello");
        }catch(Exception e){
            assertTrue(false);
        }finally{
            executorService.shutdown();
        }
    }

    @Test
    public void t3(){
        // Se puede construir un CompletableFuture a partir de una lambda Supplier (que no recibe parámetros pero sí tiene retorno)
        // con supplyAsync que retorna un completable future asinccronicamente
        CompletableFuture<String> future = CompletableFuture.supplyAsync(() -> {
            System.out.println("t3 a"+Thread.currentThread().getName());
            sleep(300);
            return "Hello";
        });
        System.out.println("t3 af"+Thread.currentThread().getName());
        try {
            String s = future.get(500, TimeUnit.MILLISECONDS);
            assertEquals(s, "Hello");
        }catch(Exception e){

            assertTrue(false);
        }
    }

    @Test
    public void t4(){
        //otra forma de obtener un completable future
        int i = 0;
        // Se puede construir un CompletableFuture a partir de una lambda (Supplier)
        // con runAsync
        Runnable r = () -> {
            sleep(300);
            System.out.println("t4 ad "+Thread.currentThread().getName());
        };

        // Note el tipo de retorno de runAsync. Siempre es un CompletableFuture<Void> asi que
        // no tenemos manera de determinar el retorno al completar el computo
        CompletableFuture<Void> voidCompletableFuture = CompletableFuture.runAsync(r);
        System.out.println("t4 af "+Thread.currentThread().getName());
        try {
            voidCompletableFuture.get(500, TimeUnit.MILLISECONDS);
        }catch(Exception e){
            assertTrue(false);
        }
    }

    @Test
    public void t5(){

        String testName = "t5";

        CompletableFuture<String> completableFuture
                = CompletableFuture.supplyAsync(() -> {
            System.out.println(testName + " - completbleFuture corriendo en el thread: "+Thread.currentThread().getName());
            return "Hello";
        });

        //thenApply acepta lambdas de aridad 1 (parametro) con retorno ,tomo el valor de la lambda anterior
        CompletableFuture<String> future = completableFuture
                .thenApply(s -> {
                    System.out.println(testName + " - future corriendo en el thread: "+Thread.currentThread().getName());

                    return s + " World";
                })
                .thenApply(s -> {
                    System.out.println(testName + " - future corriendo en el thread: "+Thread.currentThread().getName());

                    return s + "!";
                });

        try {
            assertEquals("Hello World!", future.get());
        }catch(Exception e){
            assertTrue(false);
        }
    }
    @Test
    public void t51(){

        String testName = "t5.1";

        CompletableFuture<String> completableFuture
                = CompletableFuture.supplyAsync(() -> "Hello");

        //thenApply acepta lambdas de aridad 1 con retorno
        CompletableFuture<String> future = completableFuture
                .thenApplyAsync(s -> {
                    System.out.println(testName + " - future corriendo en el thread: "+Thread.currentThread().getName());

                    return s + " World";
                })
                .thenApplyAsync(s -> {
                    System.out.println(testName + " - future corriendo en el thread: "+Thread.currentThread().getName());

                    return s + "!";
                });

        try {
            assertEquals("Hello World!", future.get());
        }catch(Exception e){
            assertTrue(false);
        }
    }


    @Test
    public void t6(){

        Date d=new Date();
        String newstring = new SimpleDateFormat("HH-mm-ss.SSS").format(d);


        String testName = "t6";

        CompletableFuture<String> completableFuture
                = CompletableFuture.supplyAsync(() -> {

            System.out.println("t6 1 "+newstring);
            System.out.println(testName + " - completbleFuture corriendo en el thread: "+Thread.currentThread().getName());

            return "Hello";
        });

        // thenAccept solo acepta Consumer (lambdas de aridad 1 que no tienen retorno)
        // analice el segundo thenAccept ¿Tiene sentido?
        CompletableFuture<Void> future = completableFuture
                .thenAccept(s -> {
                    sleep(30);
                    Date d2=new Date();
                    String newstring2 = new SimpleDateFormat("HH-mm-ss.SSS").format(d2);
                    System.out.println("t6 2 "+newstring2);
                    System.out.println(testName + " - future corriendo en el thread: " + Thread.currentThread().getName() + " lo que viene del futuro es: "+s);

                })
                .thenAccept(s -> {
                    sleep(30);
                    Date d3=new Date();
                    String newstring3 = new SimpleDateFormat("HH-mm-ss.SSS").format(d3);
                    System.out.println("t6 3 "+newstring);
                    System.out.println(testName + " - future corriendo en el thread: " + Thread.currentThread().getName() + " lo que viene del futuro es: "+s);

                });

    }

    @Test
    public void t7(){

        String testName = "t7";

        CompletableFuture<String> completableFuture = CompletableFuture.supplyAsync(() -> {
            Date d2=new Date();
            String newstring2 = new SimpleDateFormat("HH-mm-ss.SSS").format(d2);
            System.out.println("t7 1 "+newstring2);
            System.out.println(testName + " - completbleFuture corriendo en el thread: "+Thread.currentThread().getName());

            return "Hello";
        });


        CompletableFuture<Void> future = completableFuture
                .thenRun(() -> {
                    Date d2=new Date();
                    String newstring2 = new SimpleDateFormat("HH-mm-ss.SSS").format(d2);
                    System.out.println("t7 2 "+newstring2);

                    System.out.println(testName + " - future corriendo en el thread: " + Thread.currentThread().getName());
                })
                .thenRun(() -> {
                    Date d2=new Date();
                    String newstring2 = new SimpleDateFormat("HH-mm-ss.SSS").format(d2);
                    System.out.println("t7 3 "+newstring2);

                    System.out.println(testName + " - future corriendo en el thread: " + Thread.currentThread().getName());
                });

    }
    @Test
    public void t71(){

        String testName = "t7.1";

        CompletableFuture<String> completableFuture = CompletableFuture.supplyAsync(() -> {
            Date d2=new Date();
            String newstring2 = new SimpleDateFormat("HH-mm-ss.SSS").format(d2);
            System.out.println("t7 1 "+newstring2);
            System.out.println(testName + " - completbleFuture corriendo en el thread: "+Thread.currentThread().getName());

            return "Hello";
        });


        CompletableFuture<Void> future = completableFuture
                .thenRunAsync(() -> {
                    Date d2=new Date();
                    String newstring2 = new SimpleDateFormat("HH-mm-ss.SSS").format(d2);
                    System.out.println("t7.1 2 "+newstring2);

                    System.out.println(testName + " - future corriendo en el thread: " + Thread.currentThread().getName());
                })
                .thenRunAsync(() -> {
                    Date d2=new Date();
                    String newstring2 = new SimpleDateFormat("HH-mm-ss.SSS").format(d2);
                    System.out.println("t7.1 3 "+newstring2);

                    System.out.println(testName + " - future corriendo en el thread: " + Thread.currentThread().getName());
                });

    }
    @Test
    public void t72(){

        String testName = "t7.2";

        ExecutorService executorService = Executors.newCachedThreadPool();

        CompletableFuture<String> completableFuture = CompletableFuture.supplyAsync(() -> {
            Date d2=new Date();
            String newstring2 = new SimpleDateFormat("HH-mm-ss.SSS").format(d2);
            System.out.println("t7.2 1 "+newstring2);
            System.out.println(testName + " - completbleFuture corriendo en el thread: "+Thread.currentThread().getName());

            return "Hello";
        });


        CompletableFuture<Void> future = completableFuture
                .thenRunAsync(() -> {
                    Date d2=new Date();
                    String newstring2 = new SimpleDateFormat("HH-mm-ss.SSS").format(d2);
                    System.out.println("t7.2 2 "+newstring2);

                    System.out.println(testName + " - future corriendo en el thread: " + Thread.currentThread().getName());
                },executorService)
                .thenRunAsync(() -> {
                    Date d2=new Date();
                    String newstring2 = new SimpleDateFormat("HH-mm-ss.SSS").format(d2);
                    System.out.println("t7.2 3 "+newstring2);

                    System.out.println(testName + " - future corriendo en el thread: " + Thread.currentThread().getName());
                },executorService);

    }

    @Test
    public void t8(){

        String testName = "t8";

        CompletableFuture<String> completableFuture = CompletableFuture
                .supplyAsync(() -> {
                    System.out.println(testName + " - future corriendo en el thread: " + Thread.currentThread().getName());
                    return "Hello";
                })
                .thenCompose(s -> {
                    System.out.println(testName + " - compose corriendo en el thread: " + Thread.currentThread().getName());
                    return CompletableFuture.supplyAsync(() ->{
                        System.out.println(testName + " - CompletableFuture interno corriendo en el thread: " + Thread.currentThread().getName());
                        return s + " World"  ;
                    } );
                });
        System.out.println(testName + "fuera del thread " + Thread.currentThread().getName());
        try {
            assertEquals("Hello World", completableFuture.get());
        }catch(Exception e){
            assertTrue(false);
        }
    }

    @Test
    public void t9(){

        String testName = "t9";


        // El segundo parametro de thenCombina es un BiFunction la cual sí tiene que tener retorno.
        CompletableFuture<String> completableFuture = CompletableFuture

                .supplyAsync(() -> {System.out.println(testName + "fuera del thread " + Thread.currentThread().getName()); return "Hello";})
                .thenCombine(  // se usa para bifunctions

                        CompletableFuture.supplyAsync(() -> {System.out.println(testName + "fuera del thread " + Thread.currentThread().getName()); return" World";}),
                        (s1, s2) -> s1 + s2  //bi function


                );
        System.out.println(testName + "fuera del thread " + Thread.currentThread().getName());
        try {
            assertEquals("Hello World", completableFuture.get());
        }catch(Exception e){
            assertTrue(false);
        }
    }

    @Test
    public void t10(){

        String testName = "t10";

        // El segundo parametro de thenAcceptBoth debe ser un BiConsumer. No puede tener retorno.
        CompletableFuture future = CompletableFuture.supplyAsync(() -> "Hello")
                .thenAcceptBoth( // nore torna solo imprime
                        CompletableFuture.supplyAsync(() -> " World"),
                        (s1, s2) -> System.out.println(testName + " corriendo en thread: "+Thread.currentThread().getName()+ " : " +s1 + s2));

        try{
            Object o = future.get();
        }catch(Exception e){
            assertTrue(false);

        }
    }

    @Test
    public void t11(){

        String testName = "t11";

        ExecutorService es = Executors.newFixedThreadPool(1);
        CompletableFuture f = CompletableFuture.supplyAsync(()->"Hello",es);

        f.supplyAsync(() -> "Hello")
                .thenCombineAsync(
                    CompletableFuture.supplyAsync(() -> {
                        System.out.println(testName + " thenCombineAsync en Thread (1): " + Thread.currentThread().getName());
                        return " World";
                    }),
                    (s1, s2) -> {
                        System.out.println(testName + " thenCombineAsync en Thread (2): " + Thread.currentThread().getName());
                        return s1 + s2;
                    },
                    es
                );

    }

}
