package co.com.s4n.training.java.vavr;

import io.vavr.Tuple2;
import io.vavr.collection.List;
import io.vavr.collection.Tree;

import org.junit.Test;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.util.Enumeration;
import java.util.Objects;

import static org.junit.Assert.assertEquals;


public class TreeSuite {
    public interface ToTree<E> {
        Tree.Node<E> toTree();
    }

    private static <T> void prettyPrint(Tree.Node<T> treeNode, String indent, String indent2, StringBuilder s) {
        final String[] lines = treeNode.get().toString().split("\n");
        s.append(lines[0]);
        for (int i = 1; i < lines.length; i++) {
            s.append('\n')
                    .append(indent2)
                    .append(lines[i]);
        }
        for (List<Tree.Node<T>> it = treeNode.getChildren(); it.nonEmpty(); it = it.tail()) {
            final boolean isLast = it.tail().isEmpty();
            s.append('\n')
                    .append(indent)
                    .append(isLast ? "└──" : "├──");
            prettyPrint(it.head(), indent + (isLast ? "   " : "│  "), indent + (isLast ? "   " : "│  "), s);
        }
    }

    public static <T> String pp(Tree<T> tree) {
        Objects.requireNonNull(tree, "tree is null");
        if (tree.isEmpty()) { return "▣"; }
        final StringBuilder builder = new StringBuilder();
        prettyPrint((Tree.Node<T>) tree, "", "", builder);
        return builder.toString();
    }

    @Test
    public void test() {
        final Tree<String> tree =
                Tree.of("Ann",
                        Tree.of("Mary",
                                Tree.of("John",
                                        Tree.of("Avila")),
                                Tree.of("Karen"),
                                Tree.of("Steven\nAbbot\nBraddock")),
                        Tree.of("Peter",
                                Tree.of("Paul\nPalucci"),
                                Tree.of("Anthony")));

        final DefaultMutableTreeNode t = co.com.s4n.training.java.Tree.create(tree);
        @SuppressWarnings("rawtypes")
        final Enumeration v = t.preorderEnumeration();
        final StringBuilder builder = new StringBuilder();
        while (v.hasMoreElements())
            builder.append(v.nextElement()).append('\n');
        final String s = builder.toString();

        assertEquals("Ann\n" +
                "Mary\n" +
                "John\n" +
                "Avila\n" +
                "Karen\n" +
                "Steven\n" +
                "Abbot\n" +
                "Braddock\n" +
                "Peter\n" +
                "Paul\n" +
                "Palucci\n" +
                "Anthony\n" +
                "",s);
    }
    @Test
    public void shouldPrettyTreeEmpty() {

        final Tree<String> empty = Tree.empty();
        assertEquals(Tree.of(), empty);
    }
    @Test
    public void shouldPrettyTreeTest() {

        final Tree<String> tree1 = Tree.of("TEST");
        assertEquals(Tree.of("TEST"),tree1);
    }

    @Test
    public void shouldPrettyPrintTree() {

        final Tree<String> ast =
                Tree.of("IfExp",
                        Tree.of("BoolExp: false"),
                        Tree.of("IntExp: 10"),
                        Tree.of("IntExp: 20"));

        System.out.println(ast);

        final Tree<String> persons =
                Tree.of("Ann",
                        Tree.of("Mary",
                                Tree.of("John",
                                        Tree.of("Avila")),
                                Tree.of("Karen",
                                        Tree.of("Frank")),
                                Tree.of("Steven\nAbbot\nBraddock")),
                        Tree.of("Peter",
                                Tree.of("Paul\nPalucci"),
                                Tree.of("Anthony")),
                        Tree.of("Christopher",
                                Tree.of("Samuel")));

        System.out.println(persons);



    }


}
