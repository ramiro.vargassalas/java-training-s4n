package co.com.s4n.training.java;



public  class DominioSuite {

    public String s1;
    public String s2;


    public DominioSuite(){

        this.s1 = "";
        this.s2= "";

    }
    public DominioSuite(String s1, String s2){

        this.s1 = s1;
        this.s2= s2;

    }

    public static String metodo1(String s1, String s2) {

        return (s1 + s2);
    }

    public static String metodo2(String s1, String s2) {

        return (s1 + " " + s2);
    }


}