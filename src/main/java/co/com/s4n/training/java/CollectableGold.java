package co.com.s4n.training.java;

import java.awt.*;

public class CollectableGold {

    public double peso;
    public int x;
    public int y;
    public Point coordinates;

    public CollectableGold(){

        this.peso = 0;
        this.x=0;
        this.y=0;
        this.coordinates=new Point(0,0);
    }


    public CollectableGold(double peso,int x,int y){

        this.peso = peso;
        this.coordinates=new Point(x,y);
    }


    public void addGold(double peso){
        this.peso = this.peso + peso;
    }
    public void addCoordiantes(Point coordinates){
        this.coordinates = new Point(((int) (this.coordinates.getX() + coordinates.getX())),((int) (this.coordinates.getY() + coordinates.getY())));
    }
}

