package co.com.s4n.training.java;

import java.util.EnumSet;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

public class GoldCollector implements Collector<CollectableGold, CollectableGold, CollectableGold> {
    @Override
    public Supplier<CollectableGold> supplier() {
        return CollectableGold::new;
    }

    @Override
    public BiConsumer<CollectableGold, CollectableGold> accumulator() {
        return (CollectableGold p1,CollectableGold p2) -> {
            System.out.println("accumulator: "+ " "+p1.peso+" "+p1.coordinates+" "+p2.coordinates);
            p1.addCoordiantes(p2.coordinates);
            p1.addGold(p2.peso);
        };
    }

    @Override
    public BinaryOperator<CollectableGold> combiner() {
        return (CollectableGold p1, CollectableGold p2) -> {
            System.out.println("combiner: "+ p1.peso+p2.peso+p1.coordinates+p2.coordinates);
            return new CollectableGold( p1.peso+p2.peso,(int) (p1.coordinates.getX())+(int) (p2.coordinates.getX()),(int) p1.coordinates.getY()+(int) (p2.coordinates.getY()));
        };

    }

    @Override
    public Function<CollectableGold, CollectableGold> finisher() {
        return (CollectableGold p1) -> {
            System.out.println("finisher: "+ p1.peso+p1.coordinates.getLocation());

            return p1;
        };
    }

    @Override
    public Set<Characteristics> characteristics() {
        return EnumSet.of(Characteristics.UNORDERED);
    }
}