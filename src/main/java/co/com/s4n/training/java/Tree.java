package co.com.s4n.training.java;


import javax.swing.tree.DefaultMutableTreeNode;

public class Tree {

    public static <T> DefaultMutableTreeNode create(io.vavr.collection.Tree<T> tree) {
        DefaultMutableTreeNode mt = new DefaultMutableTreeNode(tree.get());
        for (io.vavr.collection.Tree.Node<T> child : tree.getChildren())
            mt.add(create(child));
        return mt;
    }

}
